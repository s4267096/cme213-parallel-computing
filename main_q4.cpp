#include <random>
#include <set>
#include <iostream>
int main(){
	std::set<double> data;
	std::default_random_engine generator;
	std::normal_distribution<double> distribution(0.0, 1.0);
	double lb = 2;
	double ub = 10;
	std::set<double>::iterator lower,upper;

	for (unsigned int i = 0; i < 1000; ++i) data.insert(distribution(generator));
	lower = data.lower_bound(lb);
	upper = data.upper_bound(ub);
	fprintf(stdout, "number of the set in the range (%f, %f): %d \n", lb, ub, std::distance(lower,upper));
	std::cout << std::distance(lower,upper) << "\n";
}	
