#include <iostream>
#include <cstdio>
#include <string.h>
#include <vector>
#include <cmath>
#include <algorithm>
using namespace std;
template <typename T, class derivedMatrix> class Matrix 
{
public:
	virtual T& operator() (int, int) = 0;
	virtual derivedMatrix operator*(derivedMatrix) = 0;
	virtual derivedMatrix operator+(derivedMatrix) = 0;
	virtual derivedMatrix operator-(derivedMatrix) = 0;
	virtual int norm0() = 0;
};