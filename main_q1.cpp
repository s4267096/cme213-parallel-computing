#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <chrono>
#include <cmath>
using namespace std;
using namespace chrono;
using namespace std;
void swapcopy(double* a, double* b, int n){
	double *temp = (double *)malloc(1000000*sizeof(double));
	memcpy(temp,a,1000000*sizeof(double));
	memcpy(a,b,1000000*sizeof(double));
	memcpy(b,temp,1000000*sizeof(double));
	free(temp);
}
void swapaddress(double* a, double* b){
	double *temp = a;
	a = b;
	b = temp;
}
int main(){
	using nanoseconds = duration<double, ratio<1, 1000000000>>;
	double *a = (double *)malloc(1000000*sizeof(double));
	double *b = (double *)malloc(1000000*sizeof(double));
	//set a and b
	for(int i = 0; i != 1000000; i++){
		a[i] = 1.0;
		b[i] = 2.0;
	}
	auto start = system_clock::now();
	swapcopy(a,b,1000000);
	auto end = system_clock::now();
	int success = 1;
	for(int i = 0; i != 1000000; i++){
		if (fabs(a[i] - 2.0) > 1.0e-10) {
			fprintf(stderr, "b incorrectly swapped into a at i: %d \n",i); 
			success = 0;
		}
		if (fabs(b[i] - 1.0) > 1.0e-10) {
			fprintf(stderr, "a incorrectly swapped into b at i: %d \n",i); 
			success = 0;
		}
	}
    auto memcpy_t = nanoseconds(end-start);
	fprintf(stdout,"Time Elapsed for memcpy: %f ns \n", memcpy_t);
	if (success) fprintf(stdout, "Swap successful \n");
	else fprintf(stdout, "Swap failed \n");
	swapcopy(a,b,1000000);
	//-----------now do manual copy of addresses
	fprintf(stdout, "Addresses Before Swap: a: %p, b: %p \n", a, b);
	start = system_clock::now();
	swapaddress(a,b);
	end = system_clock::now();
	fprintf(stdout, "Addresses After Swap: b: %p, a: %p \n", b, a);
        auto memcpy_c = nanoseconds(end-start);
	fprintf(stdout,"Time Elapsed for swapping addresses: %f ns \n", memcpy_c);
        fprintf(stdout,"Ratio of memcpy to address swap: %f \n", memcpy_t/memcpy_c);
    free(a);
    free(b);

}
