#include <iostream>
#include <random>
#include <vector>

#include <math.h>
#include <omp.h>
#include <stdlib.h>
#include <sys/time.h>

typedef unsigned int uint;
const uint kMaxInt = 100;
const uint kSize = 30000000;

std::vector<uint> serialSum(const std::vector<uint>& a, const std::vector<uint>& b) {
    std::vector<uint> sum(a.size());
    for(uint i = 0; i < a.size(); ++i){
        sum[i] = a[i] + b[i];
    }
    return sum;
}

std::vector<uint> parallelSum(const std::vector<uint>& a, const std::vector<uint>& b) {
    std::vector<uint> sum(a.size());
    #pragma omp simd 
    for(uint i = 0; i < a.size(); ++i){
        sum[i] = a[i] + b[i];
    }
    return sum;
}


std::vector<uint> initializeRandomly(const uint size, const uint max_int) {
    std::vector<uint> res(size);
    std::default_random_engine generator;
    std::uniform_int_distribution<uint> distribution(0, max_int);

    for(uint i = 0; i < size; ++i) {
        res[i] = distribution(generator);
    }

    return res;
}

int main() {

    // You can uncomment the line below to make your own simple tests
    // std::vector<uint> v = ReadVectorFromFile("vec");
    std::vector<uint> v1 = initializeRandomly(kSize, kMaxInt);
    std::vector<uint> v2 = initializeRandomly(kSize, kMaxInt);
    omp_set_num_threads(8);
    std::cout << "Parallel" << std::endl;
    struct timeval start, end;
    gettimeofday(&start, NULL);
    std::vector<uint> sums = parallelSum(v1,v2);
    std::cout << "Sum First: " << sums[0] << std::endl;
    std::cout << "Sum Second: " << sums[1] << std::endl;
    gettimeofday(&end, NULL);
    double delta = ((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec -
                    start.tv_usec) / 1.e6;
    std::cout << "Time: " << delta << std::endl;

    std::cout << "Serial" << std::endl;
    gettimeofday(&start, NULL);
    std::vector<uint> sumsSer = serialSum(v1,v2);
    std::cout << "Sum First: " << sumsSer[0] << std::endl;
    std::cout << "Sum Second: " << sumsSer[1] << std::endl;
    gettimeofday(&end, NULL);
    delta = ((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec -
             start.tv_usec) / 1.e6;
    std::cout << "Time: " << delta << std::endl;

    return 0;
}

