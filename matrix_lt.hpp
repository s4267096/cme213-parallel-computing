#include <iostream>
#include <cstdio>
#include <string.h>
#include <vector>
#include <cmath>
#include <algorithm>
using namespace std;
template <typename T, class derivedMatrix> class Matrix 
{
public:
	virtual T& operator() (int, int) = 0;
	virtual derivedMatrix operator*(derivedMatrix) = 0;
	virtual derivedMatrix operator+(derivedMatrix) = 0;
	virtual derivedMatrix operator-(derivedMatrix) = 0;
	virtual int norm0() = 0;
};
template <typename T>
class LowerDiagMatrix : public Matrix<T,LowerDiagMatrix<T>>
{
public:
	int N, dim;
	LowerDiagMatrix(int n){
		N = n*(n+1)/2;
		dim = n;
		A = (T *)malloc(N*sizeof(T));
		memset(A,0,N*sizeof(T));
	}
	T& operator() (int i, int j){
		if(j > i) {fprintf(stderr, "LowerDiagMatrix is lower diagonal i <= j in A(i,j) \n"); exit(-1);}
		return A[i*(i+1)/2 + j];
	}
	T& operator[](int i){
		return A[i];
	}
	int norm0(){
		return count_if(A,A+N,[](int i){return fabs(i) > 1e-20;});
	}
	LowerDiagMatrix operator-(LowerDiagMatrix B){
		if(N != B.N) {fprintf(stdout, "Cannot subtract matrices of different sizes \n"); exit(-1);}
		LowerDiagMatrix result(dim);
		for (int i = 0; i!=N; i++) result[i] = A[i] - B[i];
		return result;
	}
	LowerDiagMatrix operator+(LowerDiagMatrix B){
		if(N != B.N) {fprintf(stdout, "Cannot add matrices of different sizes \n"); exit(-1);}
		LowerDiagMatrix result(dim);
		for (int i = 0; i!=N; i++) result[i] = A[i] + B[i];
		return result;
	}
	LowerDiagMatrix operator*(LowerDiagMatrix B){
		if(N != B.N) {fprintf(stdout, "Cannot multiply matrices of different sizes \n"); exit(-1);}
		LowerDiagMatrix result(dim);
		for (int i = 0; i!=N; i++){
			for(int j = 0; j != i+1; j++){
				for(int k = j; k != i+1; k++){
				 	result(i,j) += (*this)(i,k)* B(k,j);
				}
			}
		}
		return result;
	}
	bool operator==(LowerDiagMatrix B){
		LowerDiagMatrix diff = operator-(B);
		return (diff.norm0() == 0) ? true : false;
	}
private:
	T *A;
};