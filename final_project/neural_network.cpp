#include "neural_network.h"

#include <armadillo>
#include "utils/common.h"
#include "gpu_func.h"
#include "mpi.h"
#include "iomanip"

#define MPI_SAFE_CALL( call ) do {                               \
    int err = call;                                              \
    if (err != MPI_SUCCESS) {                                    \
        fprintf(stderr, "MPI error %d in file '%s' at line %i",  \
               err, __FILE__, __LINE__);                         \
        exit(1);                                                 \
    } } while(0)
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}
double norms(NeuralNetwork& nn) {
    double norm_sum = 0;

    for(int i = 0; i < nn.num_layers; ++i)  {
        norm_sum += arma::accu(arma::square(nn.W[i]));
    }

    return norm_sum;
}

void write_cpudata_tofile(NeuralNetwork& nn, int iter) {
    std::stringstream s;
    s << "Outputs/CPUmats/SequentialW0-" << iter << ".mat";
    nn.W[0].save(s.str(), arma::raw_ascii);
    std::stringstream t;
    t << "Outputs/CPUmats/SequentialW1-" << iter << ".mat";
    nn.W[1].save(t.str(), arma::raw_ascii);
    std::stringstream u;
    u << "Outputs/CPUmats/Sequentialb0-" << iter << ".mat";
    nn.b[0].save(u.str(), arma::raw_ascii);
    std::stringstream v;
    v << "Outputs/CPUmats/Sequentialb1-" << iter << ".mat";
    nn.b[1].save(v.str(), arma::raw_ascii);
}

void write_diff_gpu_cpu(NeuralNetwork& nn, int iter,
                        std::ofstream& error_file) {
    arma::mat A, B, C, D;

    std::stringstream s;
    s << "Outputs/CPUmats/SequentialW0-" << iter << ".mat";
    A.load(s.str(), arma::raw_ascii);
    double max_errW0 = arma::norm(nn.W[0]-A, "inf");///arma::norm(A, "inf");
    double L2_errW0  = arma::norm(nn.W[0]-A,2);///arma::norm(A,2);

    std::stringstream t;
    t << "Outputs/CPUmats/SequentialW1-" << iter << ".mat";
    B.load(t.str(), arma::raw_ascii);
    double max_errW1 = arma::norm(nn.W[1]-B, "inf");///arma::norm(B, "inf");
    double L2_errW1  = arma::norm(nn.W[1]-B,2);///arma::norm(B,2);

    std::stringstream u;
    u << "Outputs/CPUmats/Sequentialb0-" << iter << ".mat";
    C.load(u.str(), arma::raw_ascii);
    double max_errb0 = arma::norm(nn.b[0]-C, "inf")/arma::norm(C, "inf");
    double L2_errb0  = arma::norm(nn.b[0]-C,2)/arma::norm(C,2);

    std::stringstream v;
    v << "Outputs/CPUmats/Sequentialb1-" << iter << ".mat";
    D.load(v.str(), arma::raw_ascii);
    double max_errb1 = arma::norm(nn.b[1]-D, "inf")/arma::norm(D, "inf");
    double L2_errb1  = arma::norm(nn.b[1]-D,2)/arma::norm(D,2);

    int ow = 15;

    if(iter == 0) {
        error_file << std::left<< std::setw(ow) << "Iteration" << std::left<< std::setw(
                       ow) << "Max Err W0" << std::left << std::setw(ow) << "Max Err W1"
                   << std::left<< std::setw(ow) << "Max Err b0" << std::left<< std::setw(
                       ow) << "Max Err b1" << std::left << std::setw(ow) << "L2 Err W0" << std::left
                   << std::setw(ow) << "L2 Err W1" << std::left<< std::setw(
                       ow) << "L2 Err b0" << std::left<< std::setw(ow) << "L2 Err b1" << "\n";
    }

    error_file << std::left << std::setw(ow) << iter << std::left << std::setw(
                   ow) << max_errW0 << std::left << std::setw(ow) << max_errW1 <<
               std::left << std::setw(ow) << max_errb0 << std::left << std::setw(
                   ow) << max_errb1 << std::left<< std::setw(ow) << L2_errW0 << std::left <<
               std::setw(ow) << L2_errW1 << std::left << std::setw(ow) << L2_errb0 <<
               std::left<< std::setw(ow) << L2_errb1 << "\n";

}

/* CPU IMPLEMENTATIONS */
void feedforward(NeuralNetwork& nn, const arma::mat& X, struct cache& cache) {
    cache.z.resize(2);
    cache.a.resize(2);

    // std::cout << W[0].n_rows << "\n";tw
    // if (debug) std::cout << X.cols(0,0) << "\n";
    assert(X.n_rows == nn.W[0].n_cols);
    cache.X = X;
    int N = X.n_cols;

    arma::mat z1 = nn.W[0] * X + arma::repmat(nn.b[0], 1, N);
    cache.z[0] = z1;

    arma::mat a1;
    sigmoid(z1, a1);
    cache.a[0] = a1;

    assert(a1.n_rows == nn.W[1].n_cols);
    arma::mat z2 = nn.W[1] * a1 + arma::repmat(nn.b[1], 1, N);
    cache.z[1] = z2;

    arma::mat a2;
    softmax(z2, a2);
    cache.a[1] = cache.yc = a2;
}

/*
 * Computes the gradients of the cost w.r.t each param.
 * MUST be called after feedforward since it uses the bpcache.
 * @params y : C x N one-hot column vectors
 * @params bpcache : Output of feedforward.
 * @params bpgrads: Returns the gradients for each param
 */
void backprop(NeuralNetwork& nn, const arma::mat& y, double reg,
              const struct cache& bpcache, struct grads& bpgrads) {
    bpgrads.dW.resize(2);
    bpgrads.db.resize(2);
    int N = y.n_cols;

    // std::cout << y.cols(0,0) << "\n";
    // std::cout << "backprop " << bpcache.yc << "\n";
    // std::cout << "backpropy " << y << "\n";
    arma::mat diff = (1.0 / N) * (bpcache.yc - y);
    bpgrads.dW[1] = diff * bpcache.a[0].t() + reg * nn.W[1];
    bpgrads.db[1] = arma::sum(diff, 1);
    arma::mat da1 = nn.W[1].t() * diff;

    arma::mat dz1 = da1 % bpcache.a[0] % (1 - bpcache.a[0]);

    bpgrads.dW[0] = dz1 * bpcache.X.t() + reg * nn.W[0];
    bpgrads.db[0] = arma::sum(dz1, 1);
}

/*
 * Computes the Cross-Entropy loss function for the neural network.
 */
double loss(NeuralNetwork& nn, const arma::mat& yc, const arma::mat& y,
            double reg) {
    int N = yc.n_cols;
    double ce_sum = -arma::accu(arma::log(yc.elem(arma::find(y == 1))));

    double data_loss = ce_sum / N;
    double reg_loss = 0.5 * reg * norms(nn);
    double loss = data_loss + reg_loss;
    // std::cout << "Loss: " << loss << "\n";
    return loss;
}

/*
 * Returns a vector of labels for each row vector in the input
 */
void predict(NeuralNetwork& nn, const arma::mat& X, arma::rowvec& label) {
    struct cache fcache;
    feedforward(nn, X, fcache);
    label.set_size(X.n_cols);

    for(int i = 0; i < X.n_cols; ++i) {
        arma::uword row;
        fcache.yc.col(i).max(row);
        label(i) = row;
    }
}

/*
 * Computes the numerical gradient
 */
void numgrad(NeuralNetwork& nn, const arma::mat& X, const arma::mat& y,
             double reg, struct grads& numgrads) {
    double h = 0.00001;
    struct cache numcache;
    numgrads.dW.resize(nn.num_layers);
    numgrads.db.resize(nn.num_layers);

    for(int i = 0; i < nn.num_layers; ++i) {
        numgrads.dW[i].resize(nn.W[i].n_rows, nn.W[i].n_cols);

        for(int j = 0; j < nn.W[i].n_rows; ++j) {
            for(int k = 0; k < nn.W[i].n_cols; ++k) {
                double oldval = nn.W[i](j,k);
                nn.W[i](j, k) = oldval + h;
                feedforward(nn, X, numcache);
                double fxph = loss(nn, numcache.yc, y, reg);
                nn.W[i](j, k) = oldval - h;
                feedforward(nn, X, numcache);
                double fxnh = loss(nn, numcache.yc, y, reg);
                numgrads.dW[i](j, k) = (fxph - fxnh) / (2*h);
                nn.W[i](j, k) = oldval;
            }
        }
    }

    for(int i = 0; i < nn.num_layers; ++i) {
        numgrads.db[i].resize(nn.b[i].n_rows, nn.b[i].n_cols);

        for(int j = 0; j < nn.b[i].size(); ++j) {
            double oldval = nn.b[i](j);
            nn.b[i](j) = oldval + h;
            feedforward(nn, X, numcache);
            double fxph = loss(nn, numcache.yc, y, reg);
            nn.b[i](j) = oldval - h;
            feedforward(nn, X, numcache);
            double fxnh = loss(nn, numcache.yc, y, reg);
            numgrads.db[i](j) = (fxph - fxnh) / (2*h);
            nn.b[i](j) = oldval;
        }
    }
}

/*
 * Train the neural network &nn
 */
void train(NeuralNetwork& nn, const arma::mat& X, const arma::mat& y,
           double learning_rate, double reg,
           const int epochs, const int batch_size, bool grad_check, int print_every,
           int debug) {
    int N = X.n_cols;
    int iter = 0;
    int print_flag = 0;

    for(int epoch = 0 ; epoch < epochs; ++epoch) {
        int num_batches = (N + batch_size - 1)/batch_size;

        for(int batch = 0; batch < num_batches; ++batch) {
            int last_col = std::min((batch + 1)*batch_size-1, N-1);
            arma::mat X_batch = X.cols(batch * batch_size, last_col);
            arma::mat y_batch = y.cols(batch * batch_size, last_col);

            struct cache bpcache;
            feedforward(nn, X_batch, bpcache);

            struct grads bpgrads;
            backprop(nn, y_batch, reg, bpcache, bpgrads);

            if(print_every > 0 && iter % print_every == 0) {
                if(grad_check) {
                    struct grads numgrads;
                    numgrad(nn, X_batch, y_batch, reg, numgrads);
                    assert(gradcheck(numgrads, bpgrads));
                }

                std::cout << "Loss at iteration " << iter << " of epoch " << epoch << "/" <<
                          epochs << " = " << loss(nn, bpcache.yc, y_batch, reg) << "\n";
            }

            // Gradient descent step
            for(int i = 0; i < nn.W.size(); ++i) {
                nn.W[i] -= learning_rate * bpgrads.dW[i];
            }

            for(int i = 0; i < nn.b.size(); ++i) {
                nn.b[i] -= learning_rate * bpgrads.db[i];
            }

            /* Debug routine runs only when debug flag is set. If print_every is zero, it saves
               for the first batch of each epoch to avoid saving too many large files.
               Note that for the first time, you have to run debug and serial modes together.
               This will run the following function and write out files to CPUmats folder.
               In the later runs (with same parameters), you can use just the debug flag to
               output diff b/w CPU and GPU without running CPU version */
            if(print_every <= 0) {
                print_flag = batch == 0;
            } else {
                print_flag = iter % print_every == 0;
            }
            if(debug && print_flag) {
                write_cpudata_tofile(nn, iter);
            }

            iter++;
        }
    }
}

/*
 * TODO
 * Train the neural network &nn of rank 0 in parallel. Your MPI implementation
 * should mainly be in this function.
*/
void parallel_train(NeuralNetwork& nn, const arma::mat& X, const arma::mat& y,
                    double learning_rate, double reg,
                    const int epochs, const int batch_size, bool grad_check, int print_every,
                    int debug) {
    int rank, num_procs;
    MPI_SAFE_CALL(MPI_Comm_size(MPI_COMM_WORLD, &num_procs));
    MPI_SAFE_CALL(MPI_Comm_rank(MPI_COMM_WORLD, &rank));

    int N = (rank == 0)?X.n_cols:0;
    MPI_SAFE_CALL(MPI_Bcast(&N, 1, MPI_INT, 0, MPI_COMM_WORLD));

    std::ofstream error_file;
    error_file.open("Outputs/CpuGpuDiff.txt");
    int print_flag = 0;
    //these will all be local sub sizes, except for b1
    std::vector<double*> Ws;
    std::vector<double*> bs;

    std::vector<int> Ws_n_rows;
    std::vector<int> bs_n_rows;
    std::vector<int> Ws_n_cols;
    std::vector<int> bs_n_cols;
    Ws.resize(2);
    bs.resize(2);
    Ws_n_rows.resize(2);
    bs_n_rows.resize(2);
    Ws_n_cols.resize(2);
    bs_n_cols.resize(2);
    int X_n_rows = 0;
    int X_n_cols = N;
    double* X_sub;
    double* y_sub;
    if(rank == 0){
        X_n_rows = X.n_rows;
        Ws_n_rows[0] = (nn.W[0].n_rows + num_procs - 1)/num_procs;
        bs_n_rows[0] = (nn.b[0].n_rows + num_procs - 1)/num_procs;
        Ws_n_cols[0] = nn.W[0].n_cols;
        bs_n_cols[0] = nn.b[0].n_cols;
        Ws_n_rows[1] = nn.W[1].n_rows;
        bs_n_rows[1] = nn.b[1].n_rows;
        Ws_n_cols[1] = (nn.W[1].n_cols  + num_procs - 1)/num_procs;
        bs_n_cols[1] = nn.b[1].n_cols;
        bs[1] = nn.b[1].memptr();
        X_sub = (double *) malloc(X.n_rows*X.n_cols*sizeof(double));
        y_sub = (double *) malloc(y.n_rows*y.n_cols*sizeof(double));
        std::memcpy(X_sub, X.memptr(), X.n_rows*X.n_cols*sizeof(double));
        std::memcpy(y_sub, y.memptr(), y.n_rows*y.n_cols*sizeof(double));
    }
    MPI_Bcast(&Ws_n_rows[0], 2, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&bs_n_rows[0], 2, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&Ws_n_cols[0], 2, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&bs_n_cols[0], 2, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&X_n_rows, 1, MPI_INT, 0, MPI_COMM_WORLD);
    Ws[0] = (double *) malloc(Ws_n_rows[0]*Ws_n_cols[0]*sizeof(double));
    bs[0] = (double *) malloc(bs_n_rows[0]*bs_n_cols[0]*sizeof(double));
    Ws[1] = (double *) malloc(Ws_n_rows[1]*Ws_n_cols[1]*sizeof(double));
    if (rank != 0) {
        bs[1] = (double *) malloc(bs_n_rows[1]*bs_n_cols[1]*sizeof(double));
        X_sub = (double *) malloc(X_n_cols*X_n_rows*sizeof(double));
        y_sub = (double *) malloc(Ws_n_rows[1]*X_n_cols*sizeof(double));
    }
    double* zs1 = (double *) malloc(Ws_n_rows[1]*batch_size*sizeof(double));
    double* zs1_global = (double *) malloc(Ws_n_rows[1]*batch_size*sizeof(double));
    

    MPI_SAFE_CALL(MPI_Bcast(X_sub, X_n_cols*X_n_rows, MPI_DOUBLE, 0, MPI_COMM_WORLD));
    MPI_SAFE_CALL(MPI_Bcast(y_sub, Ws_n_rows[1]*X_n_cols, MPI_DOUBLE, 0, MPI_COMM_WORLD));
    MPI_SAFE_CALL(MPI_Bcast(bs[1], bs_n_rows[1]*bs_n_cols[1], MPI_DOUBLE, 0, MPI_COMM_WORLD));
    //easy to scatter Ws[1] as it is by column
    MPI_SAFE_CALL(MPI_Scatter(nn.W[1].memptr(), Ws_n_rows[1]*Ws_n_cols[1], MPI_DOUBLE, Ws[1], Ws_n_rows[1]*Ws_n_cols[1], MPI_DOUBLE, 0, MPI_COMM_WORLD));//only works for equal things
    //easy to scatter b[0] as it is just 1 column
    MPI_SAFE_CALL(MPI_Scatter(nn.b[0].memptr(), bs_n_rows[0]*bs_n_cols[0], MPI_DOUBLE, bs[0], bs_n_rows[0]*bs_n_cols[0], MPI_DOUBLE, 0, MPI_COMM_WORLD));//only works for equal things

    int displs[num_procs];
    int sendcounts[num_procs];
    int old_displ = 0;
    int old_sendcount = 0;
    for(int col = 0; col < Ws_n_cols[0]; ++col){
        for(int i = 0; i != num_procs; ++i){
            displs[i] = old_displ + old_sendcount;
            sendcounts[i] = Ws_n_rows[0];
            old_displ = displs[i];
            old_sendcount = sendcounts[i];
            // if(rank == 0) fprintf(stdout,"i: %d, displsy: %d, sendcountsy: %d \n", i, displsy[i], sendcountsy[i]);
        }
        MPI_SAFE_CALL(MPI_Scatterv(nn.W[0].memptr(), sendcounts, displs, MPI_DOUBLE, Ws[0] + col*Ws_n_rows[0], Ws_n_rows[0], MPI_DOUBLE, 0, MPI_COMM_WORLD));
    }
   
    //declare and allocate memory
    struct nn_gpu dev_nn; 
    dev_nn.resize(2);
    // fprintf(stdout, "rank: %d, W0: %d x %d, W1: %d x %d, b0: %d x %d, b1: %d x %d \n, X: %d x %d", 
    //     rank,
    //     Ws_n_rows[0], Ws_n_cols[0],
    //     Ws_n_rows[1], Ws_n_cols[1],
    //     bs_n_rows[0], bs_n_cols[0],
    //     bs_n_rows[1], bs_n_cols[1],
    //     X_n_rows, X_n_cols);
    gpuErrchk(cudaMalloc((void**)&dev_nn.Ws[0], Ws_n_rows[0]*Ws_n_cols[0]*sizeof(double)));
    gpuErrchk(cudaMalloc((void**)&dev_nn.X, X_n_cols*X_n_rows*sizeof(double)));
    gpuErrchk(cudaMalloc((void**)&dev_nn.bs[0], bs_n_rows[0]*bs_n_cols[0]*sizeof(double)));
    gpuErrchk(cudaMalloc((void**)&dev_nn.zs[0], Ws_n_rows[0]*batch_size*sizeof(double)));
    gpuErrchk(cudaMalloc((void**)&dev_nn.as[0], Ws_n_rows[0]*batch_size*sizeof(double)));

    gpuErrchk(cudaMemcpy(dev_nn.X, X_sub, X_n_cols*X_n_rows*sizeof(double), cudaMemcpyHostToDevice));//could be pre-fetched
    // alocate second layer

    gpuErrchk(cudaMalloc((void**)&dev_nn.Ws[1], Ws_n_rows[1]*Ws_n_cols[1]*sizeof(double)));
    gpuErrchk(cudaMalloc((void**)&dev_nn.bs[1], bs_n_rows[1]*bs_n_cols[1]*sizeof(double)));
    gpuErrchk(cudaMalloc((void**)&dev_nn.zs[1], Ws_n_rows[1]*batch_size*sizeof(double)));
    gpuErrchk(cudaMalloc((void**)&dev_nn.as[1], Ws_n_rows[1]*batch_size*sizeof(double)));


    //back prop
    gpuErrchk(cudaMalloc((void**)&dev_nn.y, Ws_n_rows[1]*X_n_cols*sizeof(double)));
    gpuErrchk(cudaMalloc((void**)&dev_nn.diff, Ws_n_rows[1]*batch_size*sizeof(double)));
    gpuErrchk(cudaMemcpy(dev_nn.y, y_sub, Ws_n_rows[1]*X_n_cols*sizeof(double), cudaMemcpyHostToDevice));
    dev_nn.yc = dev_nn.as[1];
    
    gpuErrchk(cudaMalloc((void**)&dev_nn.dWs[0], Ws_n_rows[0]*Ws_n_cols[0]*sizeof(double)));
    gpuErrchk(cudaMalloc((void**)&dev_nn.dbs[0], bs_n_rows[0]*bs_n_cols[0]*sizeof(double)));
    gpuErrchk(cudaMalloc((void**)&dev_nn.dzs[0], Ws_n_rows[0]*batch_size*sizeof(double)));
    gpuErrchk(cudaMalloc((void**)&dev_nn.das[0], Ws_n_rows[0]*batch_size*sizeof(double)));

    gpuErrchk(cudaMalloc((void**)&dev_nn.dWs[1], Ws_n_rows[1]*Ws_n_cols[1]*sizeof(double)));
    gpuErrchk(cudaMalloc((void**)&dev_nn.dbs[1], bs_n_rows[1]*bs_n_cols[1]*sizeof(double)));
    gpuErrchk(cudaMalloc((void**)&dev_nn.dzs[1], Ws_n_rows[1]*batch_size*sizeof(double)));
    gpuErrchk(cudaMalloc((void**)&dev_nn.das[1], Ws_n_rows[1]*batch_size*sizeof(double)));
    /* HINT: You can obtain a raw pointer to the memory used by Armadillo Matrices
       for storing elements in a column major way. Or you can allocate your own array
       memory space and store the elements in a row major way. Remember to update the
       Armadillo matrices in NeuralNetwork &nn of rank 0 before returning from the function. */

    /* iter is a variable used to manage debugging. It increments in the inner loop
       and therefore goes from 0 to epochs*num_batches */
    gpuErrchk(cudaMemcpy(dev_nn.Ws[0], Ws[0], Ws_n_rows[0]*Ws_n_cols[0]*sizeof(double), cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpy(dev_nn.bs[0], bs[0], bs_n_rows[0]*bs_n_cols[0]*sizeof(double), cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpy(dev_nn.Ws[1], Ws[1], Ws_n_rows[1]*Ws_n_cols[1]*sizeof(double), cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpy(dev_nn.bs[1], bs[1], bs_n_rows[1]*bs_n_cols[1]*sizeof(double), cudaMemcpyHostToDevice));

    int iter = 0;
    for(int epoch = 0; epoch < epochs; ++epoch) {
        int num_batches = (X_n_cols + batch_size - 1)/batch_size;
        for(int batch = 0; batch < num_batches; ++batch) {
            int last_col = std::min((batch + 1)*batch_size, X_n_cols);
            /*
             * Possible Implementation:
             * 1. subdivide input batch of images and `MPI_scatter()' to each MPI node
             * 2. compute each sub-batch of images' contribution to network coefficient updates
             * 3. reduce the coefficient updates and broadcast to all nodes with `MPI_Allreduce()'
             * 4. update local network coefficient at each x`node
             */
            myForwardPropSigmoid(dev_nn, batch* batch_size, Ws_n_rows[0], last_col - batch * batch_size, Ws_n_cols[0]);
            myForwardPropSoftmax_part1(dev_nn, Ws_n_rows[1], last_col - batch * batch_size, Ws_n_cols[1]);          
            
            gpuErrchk(cudaMemcpy(zs1, dev_nn.zs[1], Ws_n_rows[1]*batch_size*sizeof(double), cudaMemcpyDeviceToHost));
            MPI_Allreduce(zs1, zs1_global, Ws_n_rows[1]*batch_size, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
            gpuErrchk(cudaMemcpy(dev_nn.zs[1], zs1_global, Ws_n_rows[1]*batch_size*sizeof(double), cudaMemcpyHostToDevice));

            myForwardPropSoftmax_part2(dev_nn, Ws_n_rows[1], last_col - batch * batch_size);          
            //backward prop
            myBackPropSoftmax(dev_nn, batch * batch_size, Ws_n_rows[1], Ws_n_cols[1], last_col - batch*batch_size, reg, 1.0/ ( last_col - batch * batch_size)); //some more parameters needed
            myBackPropSigmoid(dev_nn, batch * batch_size, Ws_n_rows[0], Ws_n_cols[0], last_col - batch*batch_size, reg);
            myUpdate(dev_nn.Ws[0], dev_nn.dWs[0], Ws_n_rows[0]*Ws_n_cols[0], learning_rate);
            myUpdate(dev_nn.Ws[1], dev_nn.dWs[1], Ws_n_rows[1]*Ws_n_cols[1], learning_rate);
            myUpdate(dev_nn.bs[0], dev_nn.dbs[0], bs_n_rows[0]*bs_n_cols[0], learning_rate);
            myUpdate(dev_nn.bs[1], dev_nn.dbs[1], bs_n_rows[1]*bs_n_cols[1], learning_rate);


            if(print_every <= 0) {
                print_flag = batch == 0;
            } else {
                print_flag = iter % print_every == 0;
            }
            if(print_flag){
                if(rank == 0) fprintf(stdout,"epoch: %d, batch: %d, offset: %d, batch_size: %d\n", epoch, batch, batch * batch_size, last_col - batch * batch_size);
            }

            /* Following debug routine assumes that you have already updated the arma
               matrices in the NeuralNetwork nn.  */
    
            if(debug && rank == 0 && print_flag) {
                write_diff_gpu_cpu(nn, iter, error_file);
            }

            iter++;
        }
    }
    gpuErrchk(cudaMemcpy(Ws[0], dev_nn.Ws[0], Ws_n_rows[0]*Ws_n_cols[0]*sizeof(double), cudaMemcpyDeviceToHost));
    gpuErrchk(cudaMemcpy(bs[0], dev_nn.bs[0], bs_n_rows[0]*bs_n_cols[0]*sizeof(double), cudaMemcpyDeviceToHost));
    gpuErrchk(cudaMemcpy(Ws[1], dev_nn.Ws[1], Ws_n_rows[1]*Ws_n_cols[1]*sizeof(double), cudaMemcpyDeviceToHost));
    gpuErrchk(cudaMemcpy(bs[1], dev_nn.bs[1], bs_n_rows[1]*bs_n_cols[1]*sizeof(double), cudaMemcpyDeviceToHost));
    //gather weights
    MPI_SAFE_CALL(MPI_Gather(Ws[1], Ws_n_rows[1]*Ws_n_cols[1], MPI_DOUBLE, nn.W[1].memptr(), Ws_n_rows[1]*Ws_n_cols[1], MPI_DOUBLE, 0, MPI_COMM_WORLD));//only works for equal things
    //easy to Gather b[0] as it is just 1 column
    MPI_SAFE_CALL(MPI_Gather(bs[0], bs_n_rows[0]*bs_n_cols[0], MPI_DOUBLE, nn.b[0].memptr(), bs_n_rows[0]*bs_n_cols[0], MPI_DOUBLE, 0, MPI_COMM_WORLD));//only works for equal things
    // displs[num_procs];
    // sendcounts[num_procs];
    // old_displ = 0;
    // old_sendcount = 0;
    // for(int col = 0; col < Ws_n_cols[0]; ++col){
    //     int mybatch_size = std::min(batch_size, N - (old_displ + old_sendcount) / X_n_rows);
    //     for(int i = 0; i != num_procs; ++i){
    //         displs[i] = old_displ + old_sendcount;
    //         sendcounts[i] = Ws_n_rows[0];
    //         old_displ = displs[i];
    //         old_sendcount = sendcounts[i];
    //         // if(rank == 0) fprintf(stdout,"i: %d, displsy: %d, sendcountsy: %d \n", i, displsy[i], sendcountsy[i]);
    //         MPI_SAFE_CALL(MPI_Gatherv(Ws[0] + col*Ws_n_rows[0], Ws_n_rows[0], MPI_DOUBLE, nn.W[0].memptr(), sendcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD));
    //     }
    // }
    for(int col = 0; col < Ws_n_cols[0]; ++ col){
        MPI_SAFE_CALL(MPI_Gather(Ws[0] + col*Ws_n_rows[0], Ws_n_rows[0], MPI_DOUBLE, nn.W[0].memptr() + Ws_n_rows[0]*num_procs*col, Ws_n_rows[0], MPI_DOUBLE, 0, MPI_COMM_WORLD));
    }
    cudaFree(dev_nn.Ws[0]);
    cudaFree(dev_nn.X);
    cudaFree(dev_nn.bs[0]);
    cudaFree(dev_nn.zs[0]);
    cudaFree(dev_nn.as[0]);
    cudaFree(dev_nn.Ws[1]);
    cudaFree(dev_nn.bs[1]);
    cudaFree(dev_nn.zs[1]);
    cudaFree(dev_nn.as[1]);
    cudaFree(dev_nn.y);
    cudaFree(dev_nn.diff);
    cudaFree(dev_nn.dWs[0]);
    cudaFree(dev_nn.dbs[0]);
    cudaFree(dev_nn.dzs[0]);
    cudaFree(dev_nn.das[0]);
    cudaFree(dev_nn.dWs[1]);
    cudaFree(dev_nn.dbs[1]);
    cudaFree(dev_nn.dzs[1]);
    cudaFree(dev_nn.das[1]);


    error_file.close();
}
