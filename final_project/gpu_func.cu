#include "gpu_func.h"
#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <helper_functions.h>
#include <iostream>
#include "cublas_v2.h"

#include <thrust/reduce.h>

__global__
void device_add_one(int* d_result, int t) {
    *d_result = t + 1;
}
__device__
void device_GEMM(double* A, double* B, int M, int N, int K, double* temp_row, int& A_row, int& global_column_offset){
    //each thread computes a 1x16
    //each block computes a 64 x 16 
    //each block is 16 x 4
    //implement bounds checking
    __shared__ double B_sub[16][4];//padding
    double A_sub[4] = {0.0};//local memory
    A_row = 64*blockIdx.y + threadIdx.y*blockDim.x + threadIdx.x;
    global_column_offset = 16*blockIdx.x;
    for(int offset = 0; offset < K; offset += 4){//striding down K in groups of 4
        int row = threadIdx.x % 4;
        int col = threadIdx.y * 4 + threadIdx.x / 4;
        // printf("col: %d \n", col);
        if(offset + row < K && global_column_offset + col < N){
            B_sub[col][row] = B[(offset + row) + K*(global_column_offset + col)];//very expensive
        }
        for(int k = 0; k != 4; ++k) {
            if(offset + k < K && A_row < M){//check column
                A_sub[k] = A[A_row + (offset + k)*M];//hmmm this is extremely strided access
            }
            else{
                A_sub[k] = 0.0;
            }
        }
        __syncthreads();
        for(int i_out = 0; i_out != 16; ++i_out){//do not need to bounds check they should be 0.0
            for(int k = 0; k != 4; ++k) {
                temp_row[i_out] += A_sub[k]*B_sub[i_out][k];
            }
        }
        __syncthreads();
    }

} 
template<unsigned int s>
__device__
void device_GEMM_mod(double* A, double* B, int M, int N, int K, double* temp_row, int& global_row_offset, int& global_column_offset){
    //each thread computes a 1x4
    //each block computes a 16s x 16s 
    //each block is 8 x 8
    //implement bounds checking
    __shared__ double b[16*s][8];//padding
    __shared__ double a[8][16*s];
    global_row_offset = 16*s*blockIdx.x;
    global_column_offset = 16*s*blockIdx.y;
    // printf("h: %d\n",half_warp_id);
    for(int offset = 0; offset < K; offset += 8){//striding down K in groups of 8
        //load shared memory b
        #pragma unroll
        for(unsigned int local_col_offset = 0; local_col_offset < 16*s; local_col_offset += 8){
            int row = threadIdx.x;
            int col = local_col_offset + threadIdx.y;
            if(offset + row < K && global_column_offset + col < N){
                b[col][row] = B[(offset + row) + K*(global_column_offset + col)];//very expensive
            }
        }
        //load shared memory a
        #pragma unroll
        for(unsigned int local_col_offset = 0; local_col_offset < 8; local_col_offset += 4){
            #pragma unroll
            for(unsigned int local_row_offset = 0; local_row_offset < 16*s; local_row_offset += 16){
                int row = local_row_offset +(threadIdx.y % 2)*8 + threadIdx.x;
                int col = local_col_offset + threadIdx.y / 2;
                if(global_row_offset + row < M && offset + col < K){
                    a[col][row] = A[global_row_offset + row + M*(offset + col)];//very expensive
                }
                else{
                    a[col][row] = 0.0;
                }
            }
        }
        
        __syncthreads();
        double b_elems[2*s];
        #pragma unroll
        for(int k = 0; k != 8; ++k) {
            #pragma unroll
            for(int i_out = 0; i_out != 2*s; ++ i_out) b_elems[i_out] = b[threadIdx.y + i_out*blockDim.y][k];
            #pragma unroll
            for(int local_row_offset = 0; local_row_offset != 2*s; ++local_row_offset){
                double a_elem = a[k][local_row_offset*blockDim.x +threadIdx.x];
                #pragma unroll
                for(int i_out = 0; i_out != 2*s; ++i_out){//do not need to bounds check they should be 0.0
                    // if(global_row_offset + local_row_offset*16 + half_warp_id == 1 && global_column_offset + col_id + i_out == 1) printf("%f, %f \n", a_elem, b[col_id + i_out][k]);
                    temp_row[local_row_offset + 2*s*i_out] += a_elem*b_elems[i_out];
                }
            }
        }
        __syncthreads();
    }
    global_row_offset += threadIdx.x;
    global_column_offset += threadIdx.y;
}
template<unsigned int s>
__global__
void device_GEMM_mod(double* A, double* B, double* C, double alpha, double beta, int M, int N, int K){
    double temp_row[2*s*2*s] = {0.0};
    int global_row_offset = 0;
    int global_column_offset = 0;
    device_GEMM_mod<s>(A, B, M, N, K, temp_row, global_row_offset, global_column_offset);
    #pragma unroll
    for(int local_row_offset = 0; local_row_offset != 2*s; ++local_row_offset){
        #pragma unroll
        for(int i_out = 0; i_out != 2*s; ++i_out){
            if(global_column_offset + 8*i_out < N && global_row_offset + local_row_offset*8 < M){//row has already been checked
                C[global_row_offset + local_row_offset*8 + M*(global_column_offset + 8*i_out)] = alpha*temp_row[local_row_offset + 2*s*i_out] + beta*C[global_row_offset + local_row_offset*8 + M*(global_column_offset + 8*i_out)];
            }
        }
    }
}
__global__
void device_GEMM_naive(double* A, double* B, double* C, double alpha, double beta, int M, int N, int K){
    unsigned int row = blockDim.x*blockIdx.x + threadIdx.x;
    unsigned int col = blockDim.y*blockIdx.y + threadIdx.y;
    double temp = 0.0;
    if(row < M && col < N) for(unsigned int k = 0; k != K; ++k) temp += A[row + M*k]*B[k + K*col];
    C[row + M*col] = alpha*temp + beta*C[row + M*col];
}
__global__
void device_GEMM(double* A, double* B, double* C, double alpha, double beta, int M, int N, int K){
    double temp_row[16] = {0.0};
    int A_row = 0;
    int global_column_offset = 0;
    device_GEMM(A, B, M, N, K, temp_row, A_row, global_column_offset);
    for(int i_out = 0; i_out != 16; ++i_out){
        if(global_column_offset + i_out < N && A_row < M){//row has already been checked
            C[A_row + M*(global_column_offset + i_out)] = alpha*temp_row[i_out] + beta*C[A_row + M*(global_column_offset + i_out)];
        }
    }
}    
template<unsigned int s>
__device__
void device_GEMM_T(double* A, double* BT, int M, int N, int K, double* temp_row, int& global_row_offset, int& global_column_offset){
    //here we do a multiplication by A * B = C, but our input is B
    //A = M x K, B = K x N, C = M x N, we need to take advantage of the column storage in B
    //each thread computes a 1x16
    //each block computes a 64 x 16 
    //each block is 16 x 4
    //implement bounds checking
    //each thread computes a 1x4
    //each block computes a 32s x 32s 
    //each block is 8 x 8, computes a 16 x 16
    //implement bounds checking
    __shared__ double b[8][16*s];//padding
    __shared__ double a[8][16*s];
    global_row_offset = 16*s*blockIdx.x;
    global_column_offset = 16*s*blockIdx.y;
    // printf("h: %d\n",half_warp_id);
    for(int offset = 0; offset < K; offset += 8){//striding down K in groups of 8
        //load shared memory b
        // #pragma unroll
        // for(unsigned int local_col_offset = 0; local_col_offset < 16*s; local_col_offset += 8){
        //     int row = threadIdx.x;
        //     int col = local_col_offset + threadIdx.y;
        //     if(offset + row < K && global_column_offset + col < N){
        //         b[col][row] = BT[N*(offset + row) + (global_column_offset + col)];//very expensive
        //     }
        // }
        //load shared memory b
        #pragma unroll
        for(unsigned int local_row_offset = 0; local_row_offset < 8; local_row_offset += 4){
            #pragma unroll
            for(unsigned int local_col_offset = 0; local_col_offset < 16*s; local_col_offset += 16){
                int col = local_col_offset +(threadIdx.y % 2)*8 + threadIdx.x;
                int row = local_row_offset + threadIdx.y / 2;
                if(offset + row < K && global_column_offset + col < N){
                    b[row][col] = BT[N*(offset + row) + (global_column_offset + col)];//very expensive
                }
            }
        }//load shared memory a
        #pragma unroll
        for(unsigned int local_col_offset = 0; local_col_offset < 8; local_col_offset += 4){
            #pragma unroll
            for(unsigned int local_row_offset = 0; local_row_offset < 16*s; local_row_offset += 16){
                int row = local_row_offset +(threadIdx.y % 2)*8 + threadIdx.x;
                int col = local_col_offset + threadIdx.y / 2;
                if(global_row_offset + row < M && offset + col < K){
                    a[col][row] = A[global_row_offset + row + M*(offset + col)];//very expensive
                }
                else{
                    a[col][row] = 0.0;
                }
            }
        }
        
        __syncthreads();
        double b_elems[2*s];
        #pragma unroll
        for(int k = 0; k != 8; ++k) {
            #pragma unroll
            for(int i_out = 0; i_out != 2*s; ++ i_out) b_elems[i_out] = b[k][threadIdx.y + i_out*blockDim.y];
            #pragma unroll
            for(int local_row_offset = 0; local_row_offset != 2*s; ++local_row_offset){
                double a_elem = a[k][local_row_offset*blockDim.x +threadIdx.x];
                #pragma unroll
                for(int i_out = 0; i_out != 2*s; ++i_out){//do not need to bounds check they should be 0.0
                    // if(global_row_offset + local_row_offset*16 + half_warp_id == 1 && global_column_offset + col_id + i_out == 1) printf("%f, %f \n", a_elem, b[col_id + i_out][k]);
                    temp_row[local_row_offset + 2*s*i_out] += a_elem*b_elems[i_out];
                }
            }
        }
        __syncthreads();
    }
    global_row_offset += threadIdx.x;
    global_column_offset += threadIdx.y;
} 
template<unsigned int s>
__device__
void device_T_GEMM(double* AT, double* B, int M, int N, int K, double* temp_row, int& global_row_offset, int& global_column_offset){
    //each block computes a 16s x 16s 
    //each block is 8 x 8
    //implement bounds checking
    __shared__ double b[16*s][8];
    __shared__ double a[16*s][8+1];//padding
    global_row_offset = 16*s*blockIdx.x;
    global_column_offset = 16*s*blockIdx.y;
    // printf("h: %d\n",half_warp_id);
    for(int offset = 0; offset < K; offset += 8){//striding down K in groups of 8
        //load shared memory b
        #pragma unroll
        for(unsigned int local_col_offset = 0; local_col_offset < 16*s; local_col_offset += 8){
            int row = threadIdx.x;
            int col = local_col_offset + threadIdx.y;
            if(offset + row < K && global_column_offset + col < N){
                b[col][row] = B[(offset + row) + K*(global_column_offset + col)];//very expensive
            }
        }
        //load shared memory a
        #pragma unroll
        for(unsigned int local_row_offset = 0; local_row_offset < 16*s; local_row_offset += 8){
            int col = threadIdx.x;
            int row = local_row_offset + threadIdx.y;
            if(offset + col < K && global_row_offset + row < M){
                a[row][col] = AT[(global_row_offset + row)*K + (offset + col)];//very expensive
            }
            else a[row][col] = 0.0;
        }      
        __syncthreads();
        double b_elems[2*s];
        #pragma unroll
        for(int k = 0; k != 8; ++k) {
            #pragma unroll
            for(int i_out = 0; i_out != 2*s; ++ i_out) b_elems[i_out] = b[threadIdx.y + i_out*blockDim.y][k];
            #pragma unroll
            for(int local_row_offset = 0; local_row_offset != 2*s; ++local_row_offset){
                double a_elem = a[local_row_offset*blockDim.x +threadIdx.x][k];
                #pragma unroll
                for(int i_out = 0; i_out != 2*s; ++i_out){//do not need to bounds check they should be 0.0
                    // if(global_row_offset + local_row_offset*16 + half_warp_id == 1 && global_column_offset + col_id + i_out == 1) printf("%f, %f \n", a_elem, b[col_id + i_out][k]);
                    temp_row[local_row_offset + 2*s*i_out] += a_elem*b_elems[i_out];
                }
            }
        }
        __syncthreads();
    }
    global_row_offset += threadIdx.x;
    global_column_offset += threadIdx.y;
} 
/*
Just a dummy function that can be used to warm up GPU
*/
int useless_gpu_add_one(int t) {
    int result;
    int* d_result;

    checkCudaErrors(cudaMalloc((void**)&d_result, 1 * sizeof(int)));

    event_pair timer;
    start_timer(&timer);
    device_add_one<<<1,1>>>(d_result, t);
    check_launch("device_add_one");
    double time = stop_timer(&timer);

    std::cout << "device_add_one took: " << time << " seconds" << std::endl;

    checkCudaErrors(cudaMemcpy(&result, d_result, 1 * sizeof(int),
                               cudaMemcpyDeviceToHost));
    return result;
}

/*
Routine to perform an in-place GEMM operation, i.e., C := alpha*A*B + beta*C
*/
int myGEMM(double* A, double* B, double* C, double* alpha, double* beta, int M,
           int N, int K) {
    // dim3 threads(16, 4);
    // dim3 blocks((N + 15)/16, (M+63)/64);
    // device_GEMM<<<blocks,threads>>>(A, B, C, *alpha, *beta, M, N, K);

    // dim3 threads(32, 6);
    // dim3 blocks((M + 31)/32, (N+5)/6);
    // device_GEMM_naive<<<blocks,threads>>>(A, B, C, *alpha, *beta, M, N, K);
    int k = 2;
    if (M <= 16 || N <= 16) k = 1;
    dim3 threads(8, 8);
    dim3 blocks((M + k*16-1)/k/16, (N+k*16-1)/k/16);
    switch (k){
        case 1:
            device_GEMM_mod<1><<<blocks,threads>>>(A, B, C, *alpha, *beta, M, N, K);
            break;
        case 2:
            device_GEMM_mod<2><<<blocks,threads>>>(A, B, C, *alpha, *beta, M, N, K);
            break;
    }
    check_launch("myGEMM");
    return 0;
}
template<unsigned int s>
__global__
void device_ForwardPropSigmoid(double* A, double* B, double* C, double* z, double* a, int M, int N, int K){
    //each thread computes a 1x16
    double temp_row[2*s*2*s] = {0.0};
    int global_row_offset = 0;
    int global_column_offset = 0;
    device_GEMM_mod<s>(A, B, M, N, K, temp_row, global_row_offset, global_column_offset);
    for(int local_row_offset = 0; local_row_offset != 2*s; ++local_row_offset){
        for(int i_out = 0; i_out != 2*s; ++i_out){
            if(global_column_offset + 8*i_out < N && global_row_offset + local_row_offset*8 < M){//row has already been checked
                int idx = global_row_offset + local_row_offset*8 + M*(global_column_offset + 8*i_out);
                z[idx] = temp_row[local_row_offset + 2*s*i_out] + C[global_row_offset + local_row_offset*8];
                a[idx] = 1.0/(1.0 + exp(-z[idx]));
                // if(idx % M == 0) printf("a[%d]: %16.16f\n",idx,a[idx]);
            }
        }
    }
}
void myForwardPropSigmoid(nn_gpu& dev_nn, int X_col_offset, int M, int N, int K){
    int k = 2;
    if (M <= 16 || N <= 16) k = 1;
    dim3 threads(8, 8);
    dim3 blocks((M + k*16-1)/k/16, (N+k*16-1)/k/16);
    switch (k){
        case 1:
            device_ForwardPropSigmoid<1><<<blocks,threads>>>(dev_nn.Ws[0], dev_nn.X + K*X_col_offset, dev_nn.bs[0], dev_nn.zs[0], dev_nn.as[0], M, N, K);
            break;
        case 2:
            device_ForwardPropSigmoid<2><<<blocks,threads>>>(dev_nn.Ws[0], dev_nn.X + K*X_col_offset, dev_nn.bs[0], dev_nn.zs[0], dev_nn.as[0], M, N, K);
            break;
    }
    check_launch("myForwardPropSigmoid");
}
template<unsigned int s>
__global__
void device_ForwardPropSoftmax1(double* A, double* B, double* z, int M, int N, int K){
    //each thread computes a 1x16
    double temp_row[2*s*2*s] = {0.0};
    int global_row_offset = 0;
    int global_column_offset = 0;
    device_GEMM_mod<s>(A, B, M, N, K, temp_row, global_row_offset, global_column_offset);
    for(int local_row_offset = 0; local_row_offset != 2*s; ++local_row_offset){
        for(int i_out = 0; i_out != 2*s; ++i_out){
            if(global_column_offset + 8*i_out < N && global_row_offset + local_row_offset*8 < M){//row has already been checked
                int idx = global_row_offset + local_row_offset*8 + M*(global_column_offset + 8*i_out);
                z[idx] = temp_row[local_row_offset + 2*s*i_out];
                        // if(idx % M == 0) printf("z[%d]: %16.16f\n",idx,z[idx]);
            }
        }
    }
}
__global__
void device_addbias_doelementwise_exp(double* z, double* b, double* a, int M, int N){
    //we want it sequential linear
    //maybe we will use 10*32
    unsigned idx = (blockIdx.y*gridDim.x + blockIdx.x)*blockDim.x*blockDim.y + threadIdx.y*blockDim.x + threadIdx.x;
    if(idx < M*N) {
        // if(idx % M == 0) printf("z[%d]: %16.16f\n",idx,z[idx]);
        z[idx] = z[idx] + b[idx % M];
        a[idx] = exp(z[idx]);
    }
}
template<unsigned int blockDimy>
__global__
void device_ForwardPropSoftmax2(double* a, int M, int N){
    //we want it sequential linear
    //maybe we will use 12*32
    __shared__ double col_sums[blockDimy];
    unsigned int col = blockIdx.x*blockDimy + threadIdx.y;
    if(col < N) col_sums[threadIdx.y] = a[M*col]; //this is very bad summing
    if(threadIdx.x == 0 && col < N) for(unsigned int row = 1; row != M; ++row) col_sums[threadIdx.y] += a[row + M*col]; //this is very bad summing
    unsigned int row = threadIdx.x;
    __syncthreads();
    if(row < M && col < N) {
        a[row + M*col] = a[row + M*col]/col_sums[threadIdx.y];
        // if(col == 2) printf("a[%d,%d]: %f \n", row, col,a[row+M*col]);
    }
}

void myForwardPropSoftmax_part1(nn_gpu& dev_nn, int M, int N, int K){
    int k = 2;
    if (M <= 16 || N <= 16) k = 1;
    dim3 threads(8, 8);
    dim3 blocks((M + k*16-1)/k/16, (N+k*16-1)/k/16);
    switch (k){
        case 1:
            device_ForwardPropSoftmax1<1><<<blocks,threads>>>(dev_nn.Ws[1], dev_nn.as[0], dev_nn.zs[1], M, N, K);
            break;
        case 2:
            device_ForwardPropSoftmax1<2><<<blocks,threads>>>(dev_nn.Ws[1], dev_nn.as[0], dev_nn.zs[1], M, N, K);
            break;
    }
    check_launch("myForwardPropSoftmax1");
    }
void myForwardPropSoftmax_part2(nn_gpu& dev_nn, int M, int N){
    //get da0
    dim3 threads(192);
    assert((M*N + 191)/192 < 65536);
    dim3 blocks((M*N + 191)/192, 1);
    device_addbias_doelementwise_exp<<<blocks,threads>>>(dev_nn.zs[1], dev_nn.bs[1], dev_nn.as[1] , M, N);// element wise multiplication
    check_launch("elemntwise exp");
    threads = dim3(12, 32);
    assert((N+31)/32 < 65536);
    blocks = dim3((N + 31)/32, 1);
    device_ForwardPropSoftmax2<32><<<blocks,threads>>>(dev_nn.as[1], M, N);
    check_launch("myForwardPropSoftmax2");
}
template<unsigned int s>
__global__
void device_BackProp_GEMM_T(double *diff, double *a, double* W, double *dW, int M, int N, int K, double reg){
    double temp_row[2*s*2*s] = {0.0};
    int global_row_offset;
    int global_column_offset;
    device_GEMM_T<s>(diff, a, M, N, K, temp_row, global_row_offset, global_column_offset);
    #pragma unroll
    for(int local_row_offset = 0; local_row_offset != 2*s; ++local_row_offset){
        #pragma unroll
        for(int i_out = 0; i_out != 2*s; ++i_out){
            if(global_column_offset + 8*i_out < N && global_row_offset + local_row_offset*8 < M){//row has already been checked
                dW[global_row_offset + local_row_offset*8 + M*(global_column_offset + 8*i_out)] = temp_row[local_row_offset + 2*s*i_out] + reg*W[global_row_offset + local_row_offset*8 + M*(global_column_offset + 8*i_out)];
            }
        }
    }
}
template<unsigned int s>
__global__
void device_BackProp_T_GEMM(double *W1, double* diff, double *da0, int M, int N, int K){
    double temp_row[2*s*2*s] = {0.0};
    int global_row_offset;
    int global_column_offset;
    device_T_GEMM<s>(W1, diff, M, N, K, temp_row, global_row_offset, global_column_offset);
    for(int local_row_offset = 0; local_row_offset != 2*s; ++local_row_offset){
        for(int i_out = 0; i_out != 2*s; ++i_out){
            if(global_column_offset + 8*i_out < N && global_row_offset + local_row_offset*8 < M){//row has already been checked
                da0[global_row_offset + local_row_offset*8 + M*(global_column_offset + 8*i_out)] = temp_row[local_row_offset + 2*s*i_out];
            }
        }
    }
}
__global__
void device_diff(double* x, double* y, double* diff, double invN, int N){
    //just treat it as one long gigantic list
    unsigned idx = (blockIdx.y*gridDim.x + blockIdx.x)*blockDim.x*blockDim.y + threadIdx.y*blockDim.x + threadIdx.x;
    if(idx < N) diff[idx] = invN*(x[idx] - y[idx]);
    // if(idx < 10) printf("y[%d]: %f \n", idx, y[idx]);
    // if(idx % 10 == 0 && idx < 20) printf("idx %d, diff: %f, invN: %f, x: %f, y: %f \n", idx, diff[idx], invN, x[idx], y[idx]);
}
__global__
void device_da_to_dz(double* da0, double* a0, double* dz0, int N){
    //we want it sequential linear
    //maybe we will use 10*32
    unsigned idx = (blockIdx.y*gridDim.x + blockIdx.x)*blockDim.x*blockDim.y + threadIdx.y*blockDim.x + threadIdx.x;
    if(idx < N) {
        dz0[idx] = da0[idx] * (1.0 - a0[idx])*a0[idx];
        // if(idx % 1000 == 0) printf("dz0[%d]: %16.16f \n", idx/1000, dz0[idx]);
    }

}
template<unsigned int blockSize>
__global__
void device_sum_row(double* diff, double* dB1, int M, int N, int debug = 0){
    unsigned int row = blockIdx.y*gridDim.y + blockIdx.x;//this is like a block id
    // if(threadIdx.x == 0) printf("row: %d \n", row);
    if (row < M){
        unsigned int tid = threadIdx.x;//this is like a grid id
        __shared__ double sdata[blockSize];
        // let's assume you always launch with stride0 < N
        sdata[tid] = 0.0;
        for(unsigned int idx = row + M*tid; idx < M*N; idx += M*blockSize) {
            sdata[tid] += diff[idx];
        }
        __syncthreads();
        if(blockSize >= 512) {
            if(tid < 256) {
                sdata[tid] += sdata[tid + 256];
                }
             __syncthreads();
         }
        if(blockSize >= 256) {
            if(tid < 128) {
                sdata[tid] += sdata[tid + 128];
                }
            __syncthreads();
        }
        if(blockSize >= 128) {
            if(tid < 64) {
                sdata[tid] += sdata[tid + 64];
                }
            __syncthreads();
        }
        // if(row == 0 && tid < 64 && debug) printf("%16.16f\n", sdata[tid]);

        if (tid < 32) {//todo, should not need syncthreads here
            if (blockSize >= 64) {sdata[tid] += sdata[tid + 32];__syncthreads();}
            if (blockSize >= 32) {sdata[tid] += sdata[tid + 16]; __syncthreads();}
            if (blockSize >= 16) {sdata[tid] += sdata[tid + 8];__syncthreads();}
            if (blockSize >= 8) {sdata[tid] += sdata[tid + 4];__syncthreads();}
            if (blockSize >= 4) {sdata[tid] += sdata[tid + 2];__syncthreads();}
            if (blockSize >= 2) {sdata[tid] += sdata[tid + 1];__syncthreads();}
        }
        if(tid == 0) {
            dB1[row] = sdata[0];
            // if (row < 10) printf("%16.16f \n", sdata[0]);
        }
    }
}
__global__
void device_sum_row(double* diff, double* dB1, int M, int N, int debug = 0){
    double tempsum = 0.0;
    int row = threadIdx.x;
    if(row < M){
        for(int i = 0; i < N; ++i) {
            // if(row == 0 && i < 10) printf("%d: diff[%d]: %16.16f \n ", i, row, diff[row + M*i]);
            tempsum += diff[row + M*i];
        }
        dB1[row] = tempsum;
        // if(row == 0) printf("sumrow[%d]: %f \n: ", row, tempsum);
    }
}

void myBackPropSoftmax(nn_gpu& dev_nn, int X_col_offset, int M, int N, int K, double reg, double invN)
{

    dim3 threads(192);
    assert((M*K + 191)/192 < 65536);
    dim3 blocks((M*K + 191)/192, 1);
    //do diff
    device_diff<<<blocks,threads>>>(dev_nn.as[1], dev_nn.y  + M*X_col_offset, dev_nn.diff, invN, M*K);
    check_launch("device_diff_1");
    //do first backwards propagation
    int k = 2;
    if (M <= 16 || N <= 16) k = 1;
    // std::cout << "k softmax: " << k << "\n";
    threads = dim3(8, 8);
    blocks = dim3((M + k*16-1)/k/16, (N+k*16-1)/k/16);
    switch (k){
        case 1:
            device_BackProp_GEMM_T<1><<<blocks,threads>>>(dev_nn.diff, dev_nn.as[0], dev_nn.Ws[1], dev_nn.dWs[1], M, N, K, reg);
            break;
        case 2:
            device_BackProp_GEMM_T<2><<<blocks,threads>>>(dev_nn.diff, dev_nn.as[0], dev_nn.Ws[1], dev_nn.dWs[1],  M, N, K, reg);
            break;
    }
    check_launch("backprop_gemm_T");
    //do sum to get B1
    // threads = dim3(256);
    // blocks = dim3(M);
    // assert(M < 65536);
    // // fprintf(stdout, "M: %d, K: %d\n", M, K);
    // // fprintf(stdout,"threads: %d, %d, %d \n", threads.x, threads.y, threads.z);
    // // fprintf(stdout,"blocks: %d, %d, %d \n", blocks.x, blocks.y, blocks.z);
    // device_sum_row<256><<<blocks,threads>>>(dev_nn.diff, dev_nn.dbs[1], M, K);
    threads = dim3(10);
    blocks = dim3(1);
    device_sum_row<<<blocks,threads>>>(dev_nn.diff, dev_nn.dbs[1], M, K);
    
    check_launch("device_sum_row");
    //another A.T*B
    k = 2;
    if (N <= 16 || K <= 16) k = 1;
    // std::cout << "k softmax: " << k << "\n";
    threads = dim3(8, 8);
    blocks = dim3((N + k*16-1)/k/16, (K+k*16-1)/k/16);
    switch (k){
        case 1:
            device_BackProp_T_GEMM<1><<<blocks,threads>>>(dev_nn.Ws[1], dev_nn.diff, dev_nn.das[0], N, K, M);
            break;
        case 2:
            device_BackProp_T_GEMM<2><<<blocks,threads>>>(dev_nn.Ws[1], dev_nn.diff, dev_nn.das[0], N, K, M);
            break;
    }
    check_launch("back_prop_T_gemm");
    //get da0
    threads = dim3(192);
    assert((N*K + 191)/192 < 65536);
    blocks = dim3((N*K + 191)/192, 1);
    device_da_to_dz<<<blocks,threads>>>(dev_nn.das[0], dev_nn.as[0], dev_nn.dzs[0], N*K);// element wise multiplication
    check_launch("da_dz");
}
void myBackPropSigmoid(nn_gpu& dev_nn, int X_col_offset, int M, int N, int K, double reg){
    dim3 threads = dim3(256);
    dim3 blocks = dim3(M);
    assert(M < 65536);
    device_sum_row<256><<<blocks,threads>>>(dev_nn.dzs[0], dev_nn.dbs[0], M, K, 1);
    check_launch("device_sum_row1");
    int k = 2;
    if (M <= 16 || N <= 16) k = 1;
    threads = dim3(8, 8);
    blocks = dim3((M + k*16-1)/k/16, (N+k*16-1)/k/16);
    // std::cout << "k sigmoid: " << k << "\n";
    switch (k){
        case 1:
            device_BackProp_GEMM_T<1><<<blocks,threads>>>(dev_nn.dzs[0], dev_nn.X  + N*X_col_offset, dev_nn.Ws[0], dev_nn.dWs[0], M, N, K, reg);
            break;
        case 2:
            device_BackProp_GEMM_T<2><<<blocks,threads>>>(dev_nn.dzs[0], dev_nn.X  + N*X_col_offset, dev_nn.Ws[0], dev_nn.dWs[0],  M, N, K, reg);
            break;
    }
    check_launch("backprop_sigmoid_gemmt");
    //do sum to get b0
}
__global__
void device_update(double* x, double* dx, int N, double learningrate){
    unsigned int idx = (blockIdx.y*gridDim.x + blockIdx.x)*blockDim.x*blockDim.y + threadIdx.y*blockDim.x + threadIdx.x;
    if(idx < N) {
        // printf("x[%d]: %f, dx: %f \n", idx, x[idx], dx[idx]);
        x[idx] = x[idx] - dx[idx]*learningrate;
    }
}
void myUpdate(double* x, double* dx, int N, double learningrate){
    dim3 threads(192);
    assert((N + 191)/192 < 65536);
    dim3 blocks((N + 191)/192, 1);
    device_update<<<blocks,threads>>>(x, dx, N, learningrate);// element wise multiplication
    check_launch("update");
}

