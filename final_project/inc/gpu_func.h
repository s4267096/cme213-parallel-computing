#ifndef GPU_FUNC_H_
#define GPU_FUNC_H_

#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <helper_functions.h>

//from thrust strided range example
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/functional.h>
#include <thrust/transform.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/iterator/permutation_iterator.h>

struct nn_gpu {
    double* X;
    double* y;
    double* yc;//same as as[1]
    double* diff;
    std::vector<double *> Ws;
    std::vector<double *> bs;
    std::vector<double *> zs;
    std::vector<double *> as;
    std::vector<double *> dWs;
    std::vector<double *> dbs;
    std::vector<double *> dzs;
    std::vector<double *> das;
    void resize(unsigned int n){
        Ws.resize(n);
        bs.resize(n);
        zs.resize(n);
        as.resize(n);
        dWs.resize(n);
        dbs.resize(n);
        dzs.resize(n);
        das.resize(n);
    }
};


struct event_pair {
    cudaEvent_t start;
    cudaEvent_t end;
};


inline void check_launch(const char* kernel_name) {
    cudaThreadSynchronize();
    cudaError_t err = cudaGetLastError();

    if(err != cudaSuccess) {
        std::cerr << "error in " << kernel_name << " kernel" << std::endl;
        std::cerr << "error was: " << cudaGetErrorString(err) << std::endl;
        exit(1);
    }
}

inline void start_timer(event_pair* p) {
    cudaEventCreate(&p->start);
    cudaEventCreate(&p->end);
    cudaEventRecord(p->start, 0);
}


inline double stop_timer(event_pair* p) {
    cudaEventRecord(p->end, 0);
    cudaEventSynchronize(p->end);

    float elapsed_time;
    cudaEventElapsedTime(&elapsed_time, p->start, p->end);
    cudaEventDestroy(p->start);
    cudaEventDestroy(p->end);
    return elapsed_time;
}

int useless_gpu_add_one(int t);

int myGEMM(double* A, double* B, double* C, double* alpha, double* beta, int M,
           int N, int K);

void myForwardPropSoftmax_part1(nn_gpu& dev_nn, int M, int N, int K);
void myForwardPropSoftmax_part2(nn_gpu& dev_nn, int M, int N);
void myForwardPropSigmoid(nn_gpu& dev_nn, int X_col_offset, int M, int N, int K);

void myBackPropSoftmax(nn_gpu& dev_nn, int X_col_offset, int M, int N, int K, double reg, double invN);
void myBackPropSigmoid(nn_gpu& dev_nn, int X_col_offset, int M, int N, int K, double reg);
void myUpdate(double *, double *, int, double);
#endif
