#include "matrix.hpp"
#include "matrix_lt.hpp"
using namespace std;
int main(){
	// some tests
	/*
	      [1 0 0]
	A =   [2 3 0]
	      [4 5 6]

	      [7 0 0]
	B =   [6 5 0]
	      [4 3 2]

          [ 7  0  0]
	A*B = [32 15  0]
	      [82 43 12]

	      [-6  0  0]
	A-B = [-4 -2  0]
	      [ 0  2  4]

	      [ 8  0  0]
	A+B = [ 8  8  0]
	      [ 8  8  8]
	*/
	LowerDiagMatrix<int> A(3);
	A(0,0) = 1;
	A(1,0) = 2; A(1,1) = 3;
	A(2,0) = 4; A(2,1) = 5; A(2,2) = 6;
	LowerDiagMatrix<int> B(3);
	B(0,0) = 7;
	B(1,0) = 6; B(1,1) = 5;
	B(2,0) = 4; B(2,1) = 3; B(2,2) = 2;
	LowerDiagMatrix<int> A_times_B(3);
	A_times_B(0,0) = 7;
	A_times_B(1,0) = 32; A_times_B(1,1) = 15;
	A_times_B(2,0) = 82; A_times_B(2,1) = 43; A_times_B(2,2) = 12;
	LowerDiagMatrix<int> A_minus_B(3);
	A_minus_B(0,0) = -6;
	A_minus_B(1,0) = -4; A_minus_B(1,1) = -2;
	A_minus_B(2,0) = 0; A_minus_B(2,1) = 2; A_minus_B(2,2) = 4;
	LowerDiagMatrix<int> A_plus_B(3);
	A_plus_B(0,0) = 8;
	A_plus_B(1,0) = 8; A_plus_B(1,1) = 8;
	A_plus_B(2,0) = 8; A_plus_B(2,1) = 8; A_plus_B(2,2) = 8;
	if (A.norm0() == 6) fprintf(stdout, "norm test passed \n");
	else fprintf(stdout, "norm test failed \n");
	if (A_times_B == A*B) fprintf(stdout, "multiplication test passed \n");
	else fprintf(stdout, "multiplication test failed \n");
	if (A_plus_B == A+B) fprintf(stdout, "addition test passed \n");
	else fprintf(stdout, "addition test failed \n");
	if (A_minus_B == A-B) fprintf(stdout, "subtraction test passed \n");
	else fprintf(stdout, "subtraction test failed \n");
}